/* $Id */

--- SUMMARY ---

The YAML Fields module allows module developers to specify node types,
fields, and field instances in a YAML file, rather than having to construct
them programatically.

For a full description of the module, visit the project page:
  http://drupal.org/project/yaml_fields

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/yaml_fields

--- REQUIREMENTS ---

None.

--- USAGE ---

Specify YAML Fields as a requirement in your module. Place YAML field
specifications in your module's directory as modulename.fields.yml

Below is boilerplate YAML for fields and node types.

fields:
  field_name:
    type:             # string
    entity_types:
      - foo
      - bar
    cardinality:      # int or FIELD_CARDINALITY_UNLIMITED
    translatable:     # 0|1
    locked:           # 0|1
    module:           # string
    active:           # 0|1
    deleted:          # 0|1
    settings:         # array; each field type module defines its own settings

    # The following Field API settings are not supported by the YAML Fields
    # module: columns, indexes, foreign keys, storage.

node_types:
  foo_bundle:
    name:                         # string
    base:                         # string
    description:                  # string
    help:                         # string
    custom:                       # 0|1
    modified:                     # 0|1
    locked:                       # 0|1
    disabled:                     # 0|1
    is_new:                       # 0|1
    has_title:                    # 0|1
    title_label:                  # string
    body_label:                   # string
    fields:
      field_name:
        label:                    # string
        description:              # string
        required:                 # 0|1
        default_value_function:   # string
        default_value:            # array
        deleted:                  # 0|1
        settings:                 # array; each field type module defines its own settings
        widget:
          type:                   # string
          settings:               # array; each field widget type module defines its own settings
          weight:                 # float
          module:                 # string
        display:
          default:
            label:                # string
            type:                 # string
            settings:             # array
            weight:               # float
            module:               # string
          foo_mode:
            label:
            type:
            settings:
            weight:
            module:

--- CONTACT ---

Current maintainer:
* Yitzchak Schaffer (yitznewton) - http://drupal.org/user/542796
